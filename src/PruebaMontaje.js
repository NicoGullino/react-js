import { render } from '@testing-library/react'
import React, {Component} from 'react'

class PruebaMontaje extends Component {

    constructor(props){
        console.log('constructor');
        super(props)//método constructor del Component
        //Inicializamos nuestro state de nuestro componente
        this.state = {mensajeInicial : 'Mensaje Inicial'}
        //bindeamos el contexto al método
    }

    handleClick = () => {
        this.setState({mensajeInicial : 'Mensaje Cambiado'});
    }

    render(){
        console.log('render');
        return( 
        <div className="Montaje">
            <h4>Ciclo de Montaje: constructor</h4>
            {this.state.mensajeInicial}
            <button onClick={this.handleClick}>
                Cambiar Mensaje
            </button>
        </div>
        );
    }
}

export default PruebaMontaje;
