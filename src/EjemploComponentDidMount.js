import React, { Component } from "react";

class EjemploComponentDidMount extends Component{
    constructor(props){
        console.log("constructor de ComponentDidMount")
        super(props);
        this.state={}
    }

    componentWillMount(){
        console.log('ComponentWillMount')
    }

    componentDidMount(){
        console.log('ComponentDidMount')
    }

    render(){
        return ( 
            <div>

            </div>
        )    
    }
}
export default EjemploComponentDidMount;