import logo from './logo.svg';
import './App.css';
import { Component } from 'react';
import PruebaMontaje from './PruebaMontaje';
import EjemploComponentDidMount from './EjemploComponentDidMount';

class Hello extends Component{
  render(){
    return <h2>{this.props.title}</h2>
  }
}

class Text extends Component {
  render(){
    //Destructuring
    const { ArrayOfNumbers, 
            multiply,
            //objectWithInfo
          } = this.props; 
    const mappedNumbers = ArrayOfNumbers.map(multiply)
  
    return (
      <div>
        <p>{mappedNumbers.join(', ')}</p>
      </div>      
      )
  }
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Hello title='Hello from props'/>
        <Text 
          ArrayOfNumbers = {[2,3,10]}
          multiply = { (number) => number*4}
          isActivated
        />
        <PruebaMontaje/>
        <EjemploComponentDidMount/>
      </header>
    </div>
  );
}

export default App;
